
#a simple HEM app which do battery management
#pull data from HEM app and with respect to the state of charge
# it turn off the load after threshold value 

#developer Vinay joshi & Akshay V J
import datetime
import threading
import time


from module.hemClient import HemClient
import module.hemEmail

SERVER_PORT = 9931
SERVER_NAME = '192.168.1.132'

#hemEmailAddr = 'vinaydummy0.com'
#hemPassword = 'vinaykumar123'

#Define the threshold voltage is 11.5v

THRESHOLD_VOLTAGE = 11.5

thresholdSoC_1 = 60
thresholdSoC_2 = 30
thresholdSoC_3 = 15

#Define the load node numbers
loadNodes = [2, 5, 6]
inputLoadnodes = [0]


# Create a new HEM Client
hemClient = HemClient()


server_address = (SERVER_NAME, SERVER_PORT)

V_oce = 10

V_ocf = 12.5

#Initial_charge = 0
V_intl = 0
initialFlag = 0

Initial_charge = 0

initial_SoC = 0

present_charge = 0

initialInput_charge = 0

inputPresent_charge = 0

initialDc_current = 0
present_current = 0
delta_current=0
#Data reception should always be a thread

def receiveFromServer():
	#for i in ["DCCHARGE","DCVOLT"]:
		#responseData, addressInfo = hemClient.receiveResponse()
		#batteryInitialSoC(responseData)

	#This ensures this thread is called repeatedly
	# We want to keep our ears open for any data from server
	while True:

		#Call this to get a response. 
		responseData, addressInfo = hemClient.receiveResponse()
		#print responseData
		batteryInitialSoC(responseData)
#This is a function. Call this only when you need to send a request to server
def sendToServer(apiRequest, serverAddress):

	hemClient.sendRequest(apiRequest, serverAddress)


#This is your app intelligence. You can define more functions like this 
# to handle the logic and intelligence part of your app
def batteryInitialSoC(charge_data):

	global initialFlag
	
	global Initial_charge

	global initial_SoC

	global present_charge

	global initialInput_charge

	global inputPresent_charge

	global initialDc_current
	global present_current
	#for i in ["DCCHARGE","DCVOLT"]:
	global delta_current
	if(initialFlag== 0):


		if charge_data.keys()[0] == 'DCVOLT':##	

			#print "toatal initial voltage","  is  ",charge_data.values()[0]
		
			V_intl = float(charge_data.values()[0])##
		
			initial_SoC = ((V_intl - V_oce)/(V_ocf - V_oce))*100##

			#print "the battery initial state of charge is  ", initial_SoC  ,"%"

			#initialFlag = 1


																						#until initial
	#else:
		if charge_data.keys()[0] == 'DCCURRENT':

			#present_charge = 0
			for i, val in enumerate(charge_data.values()[0]):

		#This if checks if the nodes you're iterating through belongs 
		#to one of the load nodes
				if(i in loadNodes):
					present_current = present_current + float(val)
			delta_current = float(present_current)-float(initialDc_current)
			initialDc_current = float(present_current)
			#print delta_current

		elif charge_data.keys()[0] == 'DCCHARGE':

			#present_charge = 0
			for i, val in enumerate(charge_data.values()[0]):

		#This if checks if the nodes you're iterating through belongs 
		#to one of the load nodes
				if(i in loadNodes):
					present_charge = present_charge + float(val)
			#print "o p c",present_charge

			delta_charge = float(present_charge)-float(Initial_charge)
			

			Initial_charge = float(present_charge)
																				#outup present charge
		#if charge_data.keys()[0] == 'DCCHARGE':
			for i, val in enumerate(charge_data.values()[0]):

		#This if checks if the nodes you're iterating through belongs 
		#to one of the load nodes
				if(i in inputLoadnodes):
					inputPresent_charge = inputPresent_charge + float(val)

			inputDelta_charge=float(inputPresent_charge)-float(initialInput_charge)

			#print "in in ch",initialInput_charge
			#print "input Pr ch",inputPresent_charge

			#print "i delta",inputDelta_charge

			initialInput_charge = float(inputPresent_charge)

			#print "i present",inputPresent_charge


			
			#totalDelta_charge = float(delta_charge)###

			percentDelta_charge =(float(delta_charge)/(7.5*36))

			#print "percent delta charge",percentDelta_charge
			totalPresentDelta_charge = float(percentDelta_charge)
			#time.sleep(1)

			finalSoC=initial_SoC-totalPresentDelta_charge

			if float(finalSoC) > 100:
				finalSoC = 100
			print "the present final state of ",finalSoC

			present_voltage = ((finalSoC/100)*(V_ocf - V_oce))+V_oce

			print "present voltage is", present_voltage



			if float(finalSoC) < float(thresholdSoC_1):

				sendToServer("/api/turnoff/6", server_address)
				print "heavy load turned off"


			if float(finalSoC) < float(thresholdSoC_2):
				sendToServer("/api/turnoff/5", server_address)
				print "only critical load is running"

			if float(finalSoC) < float(thresholdSoC_3):

				sendToServer("/api/turnoff/all", server_address)
				print "everything is turned off"
				loadff = "everything is turned off"
				module.hemEmail.sendEmail('hembatmapp', 'avbatmapp', 'vinaykumarjoshi111@gmail.com', 'Message from BATMAPP', '\nbattey soc is below 15 so. every node is turned off sorry for the inconvinience -from BATMAP\n\n')
			print""

			from datetime import datetime
			now= datetime.now()
		

			file1=open('storeSOC.csv','a')
			file1.write('SOC: '+str(finalSoC)+', VOLT: '+str(present_voltage) +' ,CURRENT: '+str(delta_current)+ ',NODE: BAT, DATE:'+str(now.month)+'/'+str(now.day)+'/'+str(now.year)+' ,TIME:'+str(now.hour)+':'+str(now.minute)+':'+str(now.second)+'\n')
			file1.close()


		
def main():
	#time.sleep(5)
	threads = []
	t = threading.Thread(target=receiveFromServer)
	threads.append(t)
	t.start()
	global hemEmailAddr, hemPassword

	#hemEmailAddr, hemPassword = module.hemEmail.getEmailLoginDetails('loginDetails.log')
	#Use this to send a request to server to pull power data for all nodes
	#Get this data every 3 seconds
	while True:
		sendToServer("/api/getdccharge/all", server_address)
		#time.sleep(.5)
		sendToServer("/api/getdcvoltage/2", server_address)

		sendToServer("/api/getdccurrent/all", server_address)
		#get the data from server every 3 secs. 
		#You can change it to the periodicity you want
		time.sleep(5)

if __name__ == "__main__":
    main() 
