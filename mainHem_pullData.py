#
# A sample HEM app to pull data from HEMApp when app is running on the same HEM

# Author: Ashray Manur

import datetime
import threading
import time


from module.hemClient import HemClient

SERVER_PORT = 9931
SERVER_NAME = '192.168.1.28'

# Create a new HEM Client
hemClient = HemClient()


#Configure the server address 
# If you're running your app on a HEM and pulling data from same HEM, use SERVER_NAME='localhost'
#If you're running your app on one HEM and pulling data from other HEMs, SERVER_NAME will not be 'localhost'
# It would be an ip address. For example SERVER_NAME = 192.168.1.28 

server_address = (SERVER_NAME, SERVER_PORT)

#Data reception should always be a thread
def receiveFromServer():

	#This ensures this thread is called repeatedly
	# We want to keep our ears open for any data from server
	while True:

		#Call this to get a response. 
		responseData, addressInfo = hemClient.receiveResponse()

		#responseData is a dict. It has a key value pair
		#They key indicates the type of value - Is it DC Power, AC Power, DC voltage etc. 
		#The value part of it gives the numerical value

		#prints the entire dict. Uncomment below to see entire response
		print responseData
		print addressInfo

		#see key 
		print responseData.keys() 
		#Sample response below
		#[u'DCPOWER'] This indicates the value is of type 'DCPOWER'
		#The u- prefix just means that you have a Unicode string. When you really use the string, it won't appear in your data. 


		#see value 
		print responseData.values()[0]
		#It returns a list of list
		#So just extract the first element in the outer list (which is also a list)

		#Sample response below
		#[0.20475, 5, 0, 0, 0, 0, 0, 0]
		#They are in the order of Node 0,1,2,3,4,5,6,7
		#0.20475 corresponds to Node 0
		#5 corresponds to Node 1 and so on. This is consistent with all API calls
		# You can iterate through this list to get values for all nodes


		print addressInfo[0]
		#192.168.1.28
		#This is the IP address of the server
		#This info is useful when you're sending requests to many HEMApp servers 
		# and you want to identify which server the response came from


		print addressInfo[1]
		#9931
		#This gives the port number of the server
		#This more or less remains constant for most HEMApp servers

#This is a function. Call this only when you need to send a request to server
def sendToServer(apiRequest, serverAddress):

	hemClient.sendRequest(apiRequest, serverAddress)



def main():

	threads = []
	t = threading.Thread(target=receiveFromServer)
	threads.append(t)
	t.start()

	#Use this to send a request to server to pull data
	sendToServer("/api/getdcpower/all", server_address)
	#The first argument is the API call. 
	# It makes a request to the server and tells the data it needs
	# There is a special format to this and a list of calls you can make
	# For ex "/api/getdcpower/all", Lets break this down
	# /api - this tells the server its accessing the api. This is standard for all API calls
	# /getdcpower - this tells the server you need dc power. Similarly there is getdcvoltage etc. There is a published list you can refer
	# /all - tells you need power values for all nodes


if __name__ == "__main__":
    main() 

