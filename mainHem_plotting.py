#
# A sample HEM App to demo plotting HEM data from HEM output csv data file

# Author: David Sehloff

import module.hemParsingData 
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os

def main():
    #specify the name of the data file in the hem/data folder
    file='data2'

	#First open the file and put it into read mode
    fh = open('data/'+file+'.csv', 'r')
    
    #specify which nodes should be plotted
    nodes = [0,2,6]
    #specify the names to show on the plots for each 
    nodeNames = ['node 0', 'node 2', 'node 6']
    #specify the data types to plot
    types = ['VOLT','POW']
    time_interval = [datatime.datetime.min,datatime.datetime.max]
    axis_labels = {'VOLT':'voltage (V)', 'POW':'power (W)', 'AMP':'current (A)', 'CHARGE':'charge (C)', 'ENERGY':'energy (Wh)'}
    #create structure for data: list of dicts for each node
    data=[dict() for node in nodes]
    for n, node in enumerate(nodes):
        for type in types:
            data[n][type]=[[], []]
    #print(data)
        
	#Now iterate through the file line by line 
	#for enumerate() see https://docs.python.org/3/library/functions.html#enumerate

    #A line in the file might look like this
    #VOLT:13.50,NODE:0,DATE:12/10/17-6:57:30 - this line gives you voltage for node 0
    for i, line in enumerate(fh):
    
        parsedObj = module.hemParsingData.lineParser(line)
        #print (parsedObj['type']) 
        #print (parsedObj['value'] )
        for n, node in enumerate(nodes):
            if int(parsedObj['node'])==node:
                for type in types:
                    if parsedObj['type']==type:
                        #print(parser.parse(parsedObj['date']))
                        #print(float(parsedObj['value']))
                        if (time_interval[0] <= parsedObj['date'] <= time_interval[1]):
                            data[n][type][0].append(parsedObj['date'])
                            data[n][type][1].append(float(parsedObj['value']))
            


    #plot the data for each type specified
    for type in types:
        #create a figure and axes for each type of data, with a subplot for each node
        fig,ax = plt.subplots(len(nodes),1,'all')
        #iterate through all nodes
        for n, node in enumerate(nodes):
            #plot the data for each node
            ax[n].plot(data[n][type][0],data[n][type][1],linewidth=0.8,label=type)
        
            ax[n].xaxis.set_major_formatter(mdates.DateFormatter('%H'))
            ax[n].xaxis.set_major_locator(mdates.HourLocator(interval=3))
            ax[n].set_ylabel(nodeNames[n])
            ax[n].set_yticklabels([])
            ax[n].minorticks_on()
            ax[n].tick_params(axis='y',which='minor',right='off',left='off')
            plt.setp(ax[n].get_yticklabels(), fontsize=8)
        plt.xlabel('time of day (hour)')
        #uncomment to create a y axis label
        #plt.text(0, 0, axis_labels[type], rotation=90, horizontalalignment='center', verticalalignment='center', multialignment='center')
        #plt.text(-2.5, 22, 'label:'+type,
        #         rotation=90,
        #         horizontalalignment='center',
        #         verticalalignment='top',
        #         multialignment='center')
        #uncomment to create a legend
        #leg=plt.legend(loc='upper center', bbox_to_anchor=(0.5, 5.1),
        #           ncol=3,fontsize=10)
        #for legobj in leg.legendHandles:
        #    legobj.set_linewidth(2.0)
        
        if not os.path.exists('../plots/'):
            os.makedirs('../plots/')
        #save the figure as an eps file
        fig.savefig('../plots/'+file+type+'.png', format='png', dpi=300)


if __name__ == "__main__":
    main()
