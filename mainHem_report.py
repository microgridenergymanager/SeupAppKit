#
# Sample app to generate a PDF report

# Author: Ashray Manur

import datetime
import threading
import time
from reportlab.pdfgen import canvas


from module.hemClient import HemClient
import module.hemParsingData

SERVER_PORT = 9931


SERVER_NAME = 'localhost'

server_address = (SERVER_NAME, SERVER_PORT)


# Create a new HEM Client
hemClient = HemClient()


#Data reception should always be a thread
def receiveFromServer():

	#This ensures this thread is called repeatedly
	# We want to keep our ears open for any data from server
	while True:

		#Call this to get a response. 
		responseData, addressInfo = hemClient.receiveResponse()

def main():

	threads = []
	t = threading.Thread(target=receiveFromServer)
	threads.append(t)
	t.start()

	#For more info on generating reprots, here is the tutorial
	#http://www.blog.pythonlibrary.org/2010/03/08/a-simple-step-by-step-reportlab-tutorial/

	#report lab user guide - https://www.reportlab.com/docs/reportlab-userguide.pdf
	rl = canvas.Canvas("../report/hemReport.pdf")
	rl.drawString(100,750,"Welcome to HEM Report!")
	rl.save()

if __name__ == "__main__":
    main() 

