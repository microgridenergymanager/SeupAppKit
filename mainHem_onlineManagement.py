#
# A sample HEM App to turn on and off nodes on multiple HEM boards as a simple example of aggregating loads for ancillary services

# Author: David Sehloff

import datetime
import threading
import time
import knapsack
import module.hemScheduleMgr

from module.hemClient import HemClient

POWER_THRESHOLD = 38

END_TEST = datetime.datetime.now()+datetime.timedelta(minutes=25)


NODE_NAMES = ('Class1', 'Class2', 'Class3')
NODE_INDICES = (3, 6, 4)
NODE_PRIORITIES = (1, 2, 3)

SERVER_PORT = 9931
SERVER_NAME = ('192.168.1.28')

updateInterval = 1

node_power = [0 for i in NODE_INDICES]
totalPower = 0

# Create a new HEM Client
hemClient = HemClient()

server_address = (SERVER_NAME, SERVER_PORT)


#Data reception should always be a thread
def receiveFromServer():

    #This ensures this thread is called repeatedly
    # We want to keep our ears open for any data from server
    while True:

        #Call this to get a response. 
        responseData, addressInfo = hemClient.receiveResponse()

        #print complete response
        #print responseData

        #print complete server details
        #print addressInfo

        #print responseData.keys()

        #print responseData.values()[0]
        #It returns a list of list
        #So just extract the first element in the outer list (which is also a list)
        powerLogger(responseData)

#This is a function. Call this only when you need to send a request to server
def sendToServer(apiRequest, serverAddress):
    hemClient.sendRequest(apiRequest, serverAddress)

def powerLogger(powerData):
    global totalPower
    if powerData.keys()[0] == 'DCPOWER':
        #print(powerData.values()[0])
        powerSum=0
        for k, index in enumerate(NODE_INDICES):
            val = float(powerData.values()[0][index])
            powerSum = powerSum+val
            node_power[k] = val
        totalPower=powerSum
        print (totalPower)

    
def onlineMgmt():
    while True:
        if (totalPower > POWER_THRESHOLD):
            value =[0 for p in NODE_PRIORITIES]
            for i, power in enumerate(node_power):
                #if node is on, assign its value to be its priority
                if power > 0:
                    value[i]=NODE_PRIORITIES[i]
            print('node_power:')
            print(node_power)
            result = knapsack.knapsack(node_power,value).solve(POWER_THRESHOLD)
            onNodes = [NODE_INDICES[i] for i in result[1]]
 
            print (result)
            for node in NODE_INDICES:
                if node not in onNodes:
                    sendToServer("/api/turnoff/"+str(node), server_address)
            time.sleep(10)
    
    
def main():

    threads = []
    t = threading.Thread(target=receiveFromServer)
    threads.append(t)
    t.start()
    
    threads = []
    t = threading.Thread(target=onlineMgmt)
    threads.append(t)
    t.start()
    
    print('Starting test:')
    print(datetime.datetime.now())
    now=datetime.datetime.now()
    mgr=module.hemScheduleMgr.hemScheduleMgr('gridbox',SERVER_NAME,SERVER_PORT, 8)
    on1=now+datetime.timedelta(seconds=5)
    on2=now+datetime.timedelta(seconds=10)
    on3=now+datetime.timedelta(seconds=15)
    mgr.appendOnTime('l3', on1)
    mgr.appendOnTime('l6', on2)
    mgr.appendOnTime('l4', on3)
    mgr.startExecution()
    print('executed schedule')
    #Use this to send a request to server to pull power data for all nodes
    #Get this data every update interval
    loopStart=time.time()
    while True:
        sendToServer("/api/getdcpower/all", server_address)
        time.sleep(updateInterval - (time.time()-loopStart) % updateInterval)
        
    TD0 = datetime.timedelta(0)
    while (datetime.datetime.now()-END_TEST < TD0):
        time.sleep(1)
        
    for node in NODE_INDICES:
        sendToServer("/api/turnoff/"+str(node), server_address)

    
if __name__ == "__main__":
    main() 

