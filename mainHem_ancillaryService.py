#
# A sample HEM App to turn on and off nodes

# Author: Ashray Manur

import datetime
import threading
import time


from module.hemClient import HemClient

SHEDDING_START1 = datetime.datetime.now()+datetime.timedelta(minutes=5)
SHEDDING_STOP1 = datetime.datetime.now()+datetime.timedelta(minutes=10)
SHEDDING_START2 = datetime.datetime.now()+datetime.timedelta(minutes=15)
SHEDDING_STOP2 = datetime.datetime.now()+datetime.timedelta(minutes=20)
END_TEST = datetime.datetime.now()+datetime.timedelta(minutes=25)

SHEDDING_PRIORITY1 = 1
SHEDDING_PRIORITY2 = 2
HEM_NAMES = ('Box', 'Cart')
NODE_NAMES = {}
NODE_INDICES = {}
NODE_PRIORITIES = {}
NODE_NAMES[HEM_NAMES[0]] = ('Class1', 'Class2', 'Class3')
NODE_INDICES[HEM_NAMES[0]] = (3, 6, 4)
NODE_PRIORITIES[HEM_NAMES[0]] = (1, 2, 3)
NODE_NAMES[HEM_NAMES[1]] = ('Class1', 'Class2')
NODE_INDICES[HEM_NAMES[1]] = (3, 4)
NODE_PRIORITIES[HEM_NAMES[1]] = (1, 2)
SERVER_PORT = 9931
SERVER_NAMES = ('192.168.1.28','192.168.1.159' )

updateInterval = 1

# Create a new HEM Client
hemClient = HemClient()
server_address = {}
for i, name in enumerate(HEM_NAMES):
    server_address[name] = (SERVER_NAMES[i], SERVER_PORT)


#Data reception should always be a thread
def receiveFromServer():

	#This ensures this thread is called repeatedly
	# We want to keep our ears open for any data from server
	while True:

		#Call this to get a response. 
		responseData, addressInfo = hemClient.receiveResponse()

		#print complete response
		print responseData

		#print complete server details
		print addressInfo

		print responseData.keys() 

		print responseData.values()[0]
		#It returns a list of list
		#So just extract the first element in the outer list (which is also a list)

#This is a function. Call this only when you need to send a request to server
def sendToServer(apiRequest, serverAddress):

	hemClient.sendRequest(apiRequest, serverAddress)


def ancillaryService(start,  stop,  priority):
    TD0 = datetime.timedelta(0)
    loopStart=time.time()
    enteredShedding = 0
    while True:
        dateTimeNow = datetime.datetime.now()
        #print(dateTimeNow)
        #Actuate if current time is after shedding start time and before stop time
        if (dateTimeNow-start >= TD0 and dateTimeNow-stop <= TD0):
            enteredShedding=1
            for hem in HEM_NAMES:
                for i, nodePriority in enumerate(NODE_PRIORITIES[hem]):
                    #print(priority, SHEDDING_PRIORITY)
                    if nodePriority <= priority:
                        #Send off signal
                        sendToServer("/api/turnoff/"+str(NODE_INDICES[hem][i]), server_address[hem])
        elif (enteredShedding == 1):
            for hem in HEM_NAMES:
                for i, nodePriority in enumerate(NODE_PRIORITIES[hem]):
                    #print(priority, SHEDDING_PRIORITY)
                    if nodePriority <= priority:
                        sendToServer("/api/turnon/"+str(NODE_INDICES[hem][i]), server_address[hem])
            return
        time.sleep(updateInterval - (time.time()-loopStart) % updateInterval)
    
    
def main():

    threads = []
    t = threading.Thread(target=receiveFromServer)
    threads.append(t)
    t.start()
    
    print('Starting test:')
    print(datetime.datetime.now())
    for hem in HEM_NAMES:
        for node in NODE_INDICES[hem]:
            sendToServer("/api/turnon/"+str(node), server_address[hem])
    
    ancillaryService(SHEDDING_START1, SHEDDING_STOP1, SHEDDING_PRIORITY1)
    
    ancillaryService(SHEDDING_START2, SHEDDING_STOP2, SHEDDING_PRIORITY2)
    
    TD0 = datetime.timedelta(0)
    while (datetime.datetime.now()-END_TEST < TD0):
        time.sleep(1)
        
    for hem in HEM_NAMES:
        for node in NODE_INDICES[hem]:
            sendToServer("/api/turnoff/"+str(node), server_address[hem])

    
if __name__ == "__main__":
    main() 

