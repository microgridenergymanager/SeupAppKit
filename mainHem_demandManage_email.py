#
# A sample HEM App to do online demand management
# Pulls data from HEMApp and if the power of the loads crosses 
# a certain threshold, non-priority loads are shut-off

# Author: Ashray Manur

import datetime
import threading
import time


from module.hemClient import HemClient
import module.hemEmail

SERVER_PORT = 9931
SERVER_NAME = 'localhost'

#Define your power threshold in 5
#In this app, the threshold is 5W
POWER_THRESHOLD = 5

#Define the load node numbers
loadNodes = [0, 1, 2, 3]

hemEmailAddr = ''
hemPassword = ''
# Create a new HEM Client
hemClient = HemClient()


server_address = (SERVER_NAME, SERVER_PORT)

#Data reception should always be a thread
def receiveFromServer():

	#This ensures this thread is called repeatedly
	# We want to keep our ears open for any data from server
	while True:

		#Call this to get a response. 
		responseData, addressInfo = hemClient.receiveResponse()

		print responseData

		print responseData.keys() 
		print responseData.values()
		#[[0.20475, 0.187344, 0, 0, 0, 0, 0, 0]]
		#Let's extract the first item in the list and send it to the demand management algorithm
		#We can do that by responseData.values()[0]

		onlineDemandManagement(responseData.values()[0])

#This is a function. Call this only when you need to send a request to server
def sendToServer(apiRequest, serverAddress):

	hemClient.sendRequest(apiRequest, serverAddress)

#This is your app intelligence. You can define more functions like this 
# to handle the logic and intelligence part of your app
def onlineDemandManagement(powerData):

	#print powerData

	#You should know which nodes are loads, source and others
	#Here you have to sum up the power for all 

	#1.iterate through the power values for the nodes
	#2. add the load power values and see if crosses a threshold
	#3. You should know which nodes are loads and what is there threshold

	#For this app, nodes 1,2,3,4 are loads
	#This is defined in the variable 'loadNodes' at the beginning of the app
	totalLoadPower = 0
	for i, val in enumerate(powerData):

		#This if checks if the nodes you're iterating through belongs 
		#to one of the load nodes
		if(i in loadNodes):
			totalLoadPower = totalLoadPower + float(val)
			#val is string. You should convert that to float by doing float(vals)

	#The power threshold is set using the variable POWER_THRESHOLD at the 
	#beginning of the app
	if(totalLoadPower > POWER_THRESHOLD):
		sendToServer("/api/turnoff/3", server_address)
		module.hemEmail.sendEmail(hemEmailAddr, hemPassword, 'manur@wisc.edu', 'Message from your HEM', '\nPower threshold crossed. Turning off node 3\n\n')
		


def main():

	threads = []
	t = threading.Thread(target=receiveFromServer)
	threads.append(t)
	t.start()

	global hemEmailAddr, hemPassword

	hemEmailAddr, hemPassword = module.hemEmail.getEmailLoginDetails('loginDetails.log')
	#You can put this in a function or a thread
	#Use this to send a request to server to pull power data for all nodes
	#Get this data every 3 seconds
	while True:
		sendToServer("/api/getdcpower/all", server_address)
		#get the data from server every 3 secs. 
		#You can change it to the periodicity you want
		time.sleep(3)

if __name__ == "__main__":
    main() 

